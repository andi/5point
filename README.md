# 5point

is a Linux command line bash script that tags file(s) for deletion at a set time.<br>
When invoked it compares the current date with tagged files from the past, lists <br>
files that are due to be deleted and reminds to erase them.<br>
This files can be interactively purged or all of them in one stroke.

**Usage**:

5point ``<file>`` ``<date>``

tags ``<file>`` to be deleted at ``<date>``,<br>
then checks for outdated files, lists them and asks to delete them individually <br>
or all of them.

``<date>`` formated as date string like specified in date(1)<br>
e.g. "YYYYMMDD", "now + 2 years", "10 years" or now+3min<br>

Example: Create file named 'zombie', tag it, and delete it immediately:<br>
touch zombie; 5point zombie now

5point with no options only returns output if files are due to be deleted<br> 
5point -u ``<file>`` untags ``<file>`` from all deletion dates<br>
5point -f removes all outdated files without further questions<br>
5point -l list all tagged files and their deletion dates<br>
5point -h displays this help message<br>

``<file>`` can be used like *.pdf that tags all pdfs in the current directory, or if <br>
``<file>`` is directory, all files in that directory are tagged for deletion at the<br>
given time. files and dates that contain spaces have to be masked with quotation<br>
marks: 5point "file with spaces.txt" "now + 3 month".

**Trivia:**

Five Point Palm Exploding Heart Technique. <br>
"Quite simply, the deadliest blow in all of martial arts. He hits you with his <br>
fingertips at 5 different pressure points on your body, and then lets you walk <br>
away. But once you've taken five steps your heart explodes inside your body and <br>
you fall to the floor, dead."
